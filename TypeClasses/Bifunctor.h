#define BifunctorInstance(T, A, B, C, D, E, F, G, H, I, J) \
TypeClass(Name(T B, BifunctorC), T A)                      \
T B Bimap(T, A, B)(T A G, D (*H)(C), F (*I)(E)) {          \
    J                                                      \
}
#define Bimap(T, A, B)          Name(Name(T A, T B), Map)
