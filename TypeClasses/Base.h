typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned int   uint;
typedef unsigned long  ulong;
typedef uchar          bool;
enum {
    false = 0,
    true  = !false
};

#ifndef Allocate
    #define Allocate malloc
#endif
#ifndef Release
    #define Release free
#endif

#define TypeClass(N, T)   enum { Name(Name(T, Is), N) = true };
#define NameHelper(T, N)  T##N
#define Name(T, N)        NameHelper(T, N)
#define IsA(N, T)         enum { Name(Name(Name(N,                        \
                                                IsA),                     \
                                           T),                            \
                                      __LINE__) = Name(Name(N, Is), T) };

#define FunctionType(R, N, P) typedef R (*N) P ;
