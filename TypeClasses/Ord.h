#define OrdInstance(T, A, B, L, LE, G, GE) \
TypeClass(Ord, T)                          \
IsA(T, Eq)                                 \
bool LessThan(T)(T A, T B) {               \
    L                                      \
}                                          \
bool LessThanEq(T)(T A, T B) {             \
    LE                                     \
}                                          \
bool GreaterThan(T)(T A, T B) {            \
    G                                      \
}                                          \
bool GreaterThanEq(T)(T A, T B) {          \
    GE                                     \
}
#define LessThan(T)      Name(T, LessThan)
#define LessThanEq(T)    Name(T, LessThanEq)
#define GreaterThan(T)   Name(T, GreaterThan)
#define GreaterThanEq(T) Name(T, GreaterThanEq)
