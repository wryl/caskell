#define FunctorInstance(T, A, B, P, R, N1, N2, M) \
TypeClass(Functor(T, B), T A)                     \
T B Map(T, A, B)(T A N1,                          \
                 R (*N2)(P)) {                    \
    M                                             \
}
#define Functor(T, A) Name(T A, Functor)
#define Map(T, A, B) Name(Name(T A, T B), Map)
