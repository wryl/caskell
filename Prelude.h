#ifndef PreludeIncluded
#define PreludeIncluded

#include "TypeClasses/Base.h"
#include "TypeClasses/Eq.h"
#include "TypeClasses/Ord.h"
#include "TypeClasses/Functor.h"
#include "TypeClasses/Bifunctor.h"
#include "TypeClasses/Applicative.h"
#include "Types/Maybe.h"
#include "Types/Instances/MaybeFunctor.h"
#include "Types/Instances/MaybeApplicative.h"
#include "Types/Either.h"
#include "Types/Instances/EitherFunctor.h"
#include "Types/Instances/EitherBifunctor.h"
#include "Types/BoxedArray.h"
#include "Types/Instances/BoxFunctor.h"
#include "Types/List.h"
#include "Types/Instances/ListFunctor.h"

#endif
