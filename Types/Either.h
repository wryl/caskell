#define EitherType(T, R)                  \
typedef struct Either(T, R) Either(T, R); \
struct Either(T, R) {                     \
    bool IsLeft;                          \
    T    LeftV;                           \
    R    RightV;                          \
};                                        \
Either(T, R) Left(T, R)(T value) {        \
    Either(T, R) result;                  \
    result.IsLeft = true;                 \
    result.Left   = value;                \
    return result;                        \
}                                         \
Either(T, R) Right(T, R)(R value) {       \
    Either(T, R) result;                  \
    result.IsLeft = false;                \
    result.Right  = value;                \
    return result;                        \
}
#define Left(T, R)   Name(T, Name(R, Left))
#define Right(T, R)  Name(T, Name(R, Right))
#define Either(T, R) Name(T, Name(R, Either))
