#define MaybeFunctor(T, R)                                \
FunctorInstance(Maybe, (T), (R), T, R, maybe, function, { \
        return (maybe.IsNothing) ?                        \
               (Nothing(R))    :                          \
               (Just(R)(function(maybe.Value)));          \
})
