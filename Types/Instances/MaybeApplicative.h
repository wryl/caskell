#define MaybeApplicative(T, R)                                         \
ApplicativeFunctionType(Maybe, (T), (R), R, T)                         \
MaybeType(ApplicativeFunction(Maybe, (T), (R), R, T))                  \
ApplicativeInstance(Maybe, (T), (R), R, T,                             \
    T,     just,     { return Just(T)(just);                        }, \
    maybe, function, { return Just(R)(function.Value(maybe.Value)); }  \
)
#define JustAP(T, R) Just(ApplicativeFunction(Maybe, (T), (R), R, T))
