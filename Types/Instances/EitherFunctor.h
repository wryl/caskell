#define EitherFunctor(T, A, B)                                    \
FunctorInstance(Either, (T, A), (T, B), A, B, either, function, { \
    return (either.IsLeft == true) ?                              \
           (Left(T, B)(either.Left)) :                            \
           (Right(T, B)(function(either.Right)));                 \
})
