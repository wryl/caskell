#define MaybeType(T)              \
typedef struct Maybe(T) Maybe(T); \
struct Maybe(T) {                 \
    bool IsNothing;               \
    T    Value;                   \
};                                \
Maybe(T) Nothing(T) {             \
    Maybe(T) result;              \
    result.IsNothing = true;      \
    return result;                \
}                                 \
Maybe(T) Just(T)(T just) {        \
    Maybe(T) result;              \
    result.IsNothing = false;     \
    result.Value    = just;       \
    return result;                \
}
#define Nothing(T)   Name(T, Nothing())
#define Just(T)      Name(T, Just)
#define Maybe(T)     Name(T, Maybe)
