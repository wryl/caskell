#define BoxedArrayType(T, S, A, B, C, D, E)                \
typedef struct BoxedArray(T, S) BoxedArray(T, S);          \
struct BoxedArray(T, S) {                                  \
    T     Value;                                           \
    T*    Array;                                           \
    ulong Size;                                            \
};                                                         \
BoxedArray(T, S) NullArray(T, S) {                         \
    BoxedArray(T, S) result;                               \
    result.Array = ((void*)0);                             \
    result.Size = 0;                                       \
    return result;                                         \
}                                                          \
bool IsNullArray(T, S)(BoxedArray(T, S) box) {             \
    return (                                               \
               box.Array == ((void*)0))                    \
           );                                              \
}                                                          \
BoxedArray(T, S) CreateArray(T, S) A {                     \
    BoxedArray(T, S) result;                               \
    T*     B;                                              \
    result.Array = Allocate(sizeof(T) * S);                \
    if(IsNullArray(T, S)(result)) {                        \
        return Null(T);                                    \
    }                                                      \
    B = result.Array;                                      \
    { C }                                                  \
    result.Value = *result.Array;                          \
    result.Size  = S;                                      \
    return result;                                         \
}                                                          \
BoxedArray(T, S) DeleteArray(T, S)(BoxedArray(T, S) box) { \
    T* D;                                                  \
    if(IsNull(T)(box)) {                                   \
        return Null(T);                                    \
    }                                                      \
    else {                                                 \
        D = box.Array;                                     \
        { E }                                              \
        Release(box.Array);                                \
        box.Array = ((void*)0);                            \
        box.Size = 0;                                      \
        return Null(T);                                    \
    }                                                      \
}
#define BoxType(T, A, B, C, D, E) BoxedArrayType(T, 1, A, B, C, D, E)
#define BoxedArray(T, S)          Name(Name(T, S), Box)
#define Box(T)                    Name(Name(T, 1), Box)
#define NullArray(T, S)           Name(Name(T, S), Null)()
#define Null(T)                   Name(Name(T, 1), Null)()
#define IsNullArray(T, S)         Name(Name(T, S), IsNull)
#define IsNull(T)                 Name(Name(T, 1), IsNull)
#define CreateArray(T, S)         Name(Name(T, S), Create)
#define Create(T)                 Name(Name(T, 1), Create)
#define DeleteArray(T, S)         Name(Name(T, S), Delete)
#define Delete(T)                 Name(Name(T, 1), Delete)
